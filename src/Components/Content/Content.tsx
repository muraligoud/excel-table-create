import { useState } from "react";
import { Layout, Tabs, PageHeader, Empty, Tooltip } from "antd";
import _ from "lodash";
import { useValues, useActions } from "kea";
import { useHistory } from "react-router-dom";
import { HomeFilled } from "@ant-design/icons";

import { HeadersList, DragAndDrop } from "../DragAndDropList";
import Charts from "../Charts";
import Tables from "../Tables";
import logic from "../../logics/DataLogic";

const { Sider, Content } = Layout;

const ContentArea = () => {
  const history = useHistory();
  const { data } = useValues(logic);
  const { setItems } = useActions(logic);
  const [currentValue, setCurrentValue] = useState("");
  const [activeTab, setActiveTab] = useState("tables");

  const headerNames = _.chain(data).first().keys().value();

  return (
    <PageHeader
      className="content-area-page"
      ghost={false}
      title={
        <>
          <Tooltip title="Go To Upload" placement="topLeft">
            <HomeFilled onClick={() => history.push("/")} /> &nbsp;Create
          </Tooltip>
          <span>Tables/Charts</span>
        </>
      }
      style={{ padding: "0px", backgroundColor: "#536548" }}
      onBack={() => history.push("/excel-table")}
    >
      {_.isEmpty(headerNames) ? (
        <Empty style={{ height: "100vh", color: "black" }} />
      ) : (
        <Layout className="site-layout">
          <Sider>
            <HeadersList
              headerNames={headerNames}
              setCurrentValue={setCurrentValue}
            />
          </Sider>
          <DragAndDrop currentValue={currentValue}>
            <Content
              className="site-layout-background"
              style={{
                margin: "24px 16px",
                padding: 24,
                minHeight: 280,
              }}
            >
              <Tabs
                activeKey={activeTab}
                onChange={(tab: string) => {
                  setActiveTab(tab);
                  setItems([]);
                }}
              >
                <Tabs.TabPane tab="Tables" key="tables">
                  <Tables activeTab={activeTab} />
                </Tabs.TabPane>
                <Tabs.TabPane tab="Charts" key="charts">
                  <Charts activeTab={activeTab} />
                </Tabs.TabPane>
              </Tabs>
            </Content>
          </DragAndDrop>
        </Layout>
      )}
    </PageHeader>
  );
};

export default ContentArea;
