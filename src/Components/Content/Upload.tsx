import { Upload, PageHeader } from "antd";
import { InboxOutlined } from "@ant-design/icons";
import _ from "lodash";
import XLSX from "xlsx";
import { useActions } from "kea";
import { useHistory } from "react-router-dom";

import logic from "../../logics/DataLogic";

const UploadSheet = () => {
  const { setData } = useActions(logic);
  const history = useHistory();

  const handleFileUpload = (files: any) => {
    if (!files?.file) {
      return;
    }
    const fileReader = new FileReader();
    fileReader.onload = () => {
      const workbook = XLSX.read(fileReader.result, { type: "binary" });
      const firstSheet = workbook.SheetNames[0];
      const ws = workbook.Sheets[firstSheet];
      setData(
        XLSX.utils.sheet_to_json(ws, {
          raw: false,
        }),
        files.file.name
      );
      history.push("/excel-table");
    };
    fileReader.readAsBinaryString(files.file);
  };

  return (
    <PageHeader title="Upload">
      <div className="container-element">
        <Upload.Dragger
          accept=".xlsx"
          beforeUpload={_.stubFalse}
          onChange={handleFileUpload}
          maxCount={1}
          showUploadList={false}
        >
          <p className="ant-upload-drag-icon">
            <InboxOutlined />
          </p>
          <p className="ant-upload-text">
            Click or drag file to this area to upload
          </p>
          <p className="ant-upload-hint">Only single XLSX file is allowed.</p>
        </Upload.Dragger>
      </div>
    </PageHeader>
  );
};

export default UploadSheet;
