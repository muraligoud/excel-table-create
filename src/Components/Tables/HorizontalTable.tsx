import { Table, Result, Tooltip, Button } from "antd";
import _ from "lodash";
import { TableOutlined, ClearOutlined } from "@ant-design/icons";
import { useValues } from "kea";

import logic from "../../logics/DataLogic";

const HorizontalTable = (props: any) => {
  const { data, headerNames: columns } = useValues(logic);
  const { items, clearItems } = props;

  return _.isEmpty(items) ? (
    <Result
      icon={<TableOutlined />}
      title={`Drop here to create horizontal table`}
    />
  ) : (
    <>
      <Tooltip title="Clear">
        <Button className={"ant-btn-icon"} shape="circle" onClick={clearItems}>
          <ClearOutlined />
        </Button>
      </Tooltip>
      <Table
        dataSource={data}
        columns={_.filter(columns, (col) => _.includes(items, col.dataIndex))}
        rowKey={(record) => `${record.SalesOrderID}-${record.ProductID}`}
        bordered={true}
        pagination={false}
        scroll={{ y: `calc(100vh - 224px)` }}
      />
    </>
  );
};

export default HorizontalTable;
