import _ from "lodash";
import { TableOutlined, ClearOutlined } from "@ant-design/icons";
import { Result, Tooltip, Button } from "antd";
import { useValues } from "kea";

import logic from "../../logics/DataLogic";

const VerticalTable = (props: any) => {
  const { data } = useValues(logic);

  const { items, clearItems } = props;

  const getCells = (item: any) => {
    return _.map(data, (val, index) => (
      <td key={`${_.get(val, item)}-${index}`}>{_.get(val, item)}</td>
    ));
  };

  return (
    <>
      {_.isEmpty(items) ? (
        <Result
          icon={<TableOutlined />}
          title={`Drop here to create vertical table`}
        />
      ) : (
        <>
          <Tooltip title="Clear">
            <Button
              className={"ant-btn-icon"}
              shape="circle"
              onClick={clearItems}
              style={{ marginBottom: "5px" }}
            >
              <ClearOutlined />
            </Button>
          </Tooltip>

          <div className="table-container">
            <table>
              {_.map(items, (item) => {
                return (
                  <tr key={item}>
                    <th>{item}</th>
                    {getCells(item)}
                  </tr>
                );
              })}
            </table>
          </div>
        </>
      )}
    </>
  );
};

export default VerticalTable;
