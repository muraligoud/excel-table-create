import { useState, useEffect } from "react";
import { Select } from "antd";
import _ from "lodash";
import { useValues, useActions } from "kea";
import { useHistory } from "react-router-dom";

import HorizontalTable from "./HorizontalTable";
import VerticalTable from "./VerticalTable";
import logic from "../../logics/DataLogic";

const options = [
  { label: "Horizontal Table", value: "horizontal" },
  { label: "Vertical Table", value: "vertical" },
];

const Tables: React.FC<Record<"activeTab", string>> = ({ activeTab }) => {
  const { items } = useValues(logic);
  const { setItems, resetStorage } = useActions(logic);
  const history = useHistory();

  const [value, setValue] = useState("horizontal");
  const [horizontalItems, setHorizontalItems] = useState<any>([]);
  const [verticalItems, setVerticalItems] = useState<any>([]);

  useEffect(() => {
    if (_.get(history, "action") === "PUSH") {
      resetStorage();
    }
  }, []);

  useEffect(() => {
    if (value === "horizontal") {
      setHorizontalItems(_.uniq([...horizontalItems, ...items]));
    } else {
      setVerticalItems(_.uniq([...verticalItems, ...items]));
    }
  }, [items]);

  useEffect(() => {
    setValue("horizontal");
    setHorizontalItems([]);
    setVerticalItems([]);
  }, [activeTab]);

  const clearItems = (active: string) => {
    setItems([]);
    if (active === "horizontal") {
      setHorizontalItems([]);
    } else {
      setVerticalItems([]);
    }
  };

  return (
    <>
      <Select
        style={{ width: "150px", marginBottom: "5px", marginRight: "5px" }}
        value={value}
        options={options}
        onChange={(val) => {
          setItems([]);
          setValue(val);
        }}
      />
      {value === "horizontal" ? (
        <HorizontalTable
          items={horizontalItems}
          clearItems={() => clearItems("horizontal")}
        />
      ) : (
        <VerticalTable
          items={verticalItems}
          clearItems={() => clearItems("vertical")}
        />
      )}
    </>
  );
};

export default Tables;
