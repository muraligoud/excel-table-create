import React, { useEffect } from "react";
import _ from "lodash";
import c3 from "c3";
import { Result, Tooltip, Button } from "antd";
import { BarChartOutlined, ClearOutlined } from "@ant-design/icons";
import { useValues } from "kea";

import logic from "../../logics/DataLogic";

const BarChart: React.FC<any> = (props) => {
  const { data } = useValues<any>(logic);

  const { items, clearItems } = props;
  const columns: any = _.chain(items)
    .map((item: any) => {
      return [item, ..._.map(data, (val) => val[item])];
    })
    .filter((val) => val !== undefined)
    .value();

  useEffect(() => {
    if (!_.isEmpty(items)) {
      c3.generate({
        bindto: "#chart",
        data: {
          columns,
          type: "bar",
        },
        axis: {
          x: {
            type: "category",
            categories: _.map(data, _.first(items)) as any,
          },
        },
        zoom: {
          enabled: true,
        },
      });
    }
  }, [items, columns, data]);

  return _.isEmpty(items) ? (
    <Result
      icon={<BarChartOutlined />}
      title={`Drop here to create Bar Chart`}
    />
  ) : (
    <>
      <Tooltip title="Clear">
        <Button className={"ant-btn-icon"} shape="circle" onClick={clearItems}>
          <ClearOutlined />
        </Button>
      </Tooltip>
      <div id="chart" />
    </>
  );
};

export default BarChart;
