import { useState, useEffect } from "react";
import { Select } from "antd";
import _ from "lodash";
import { useValues, useActions } from "kea";
import { useHistory } from "react-router-dom";

import BarChart from "./BarChart";
import PieChart from "./PieChart";
import LineChart from "./LineChart";
import logic from "../../logics/DataLogic";

const options = [
  { label: "Bar Chart", value: "bar" },
  { label: "Line Chart", value: "line" },
  { label: "Pie Chart", value: "pie" },
];

const Charts = ({ activeTab }: any) => {
  const { items } = useValues(logic);
  const { setItems, resetStorage } = useActions(logic);
  const history = useHistory();

  const [value, setValue] = useState("bar");
  const [barItems, setBarItems] = useState<any>([]);
  const [lineItems, setLineItems] = useState<any>([]);
  const [pieItems, setPieItems] = useState<any>([]);

  useEffect(() => {
    if (_.get(history, "action") === "PUSH") {
      resetStorage();
    }
  }, []);

  useEffect(() => {
    switch (value) {
      case "bar":
        setBarItems(_.uniq([...barItems, ...items]));
        return;
      case "line":
        setLineItems(_.uniq([...lineItems, ...items]));
        return;
      case "pie":
        setPieItems(_.uniq([...pieItems, ...items]));
        return;
    }
  }, [items]);

  useEffect(() => {
    setValue("bar");
    setBarItems([]);
    setLineItems([]);
    setPieItems([]);
  }, [activeTab]);

  const clearItems = (active: string) => {
    setItems([]);
    if (active === "bar") {
      setBarItems([]);
    } else if (active === "line") {
      setLineItems([]);
    } else {
      setPieItems([]);
    }
  };

  const getChart = () => {
    if (value === "bar") {
      return <BarChart items={barItems} clearItems={() => clearItems("bar")} />;
    } else if (value === "line") {
      return (
        <LineChart items={lineItems} clearItems={() => clearItems("line")} />
      );
    }
    if (value === "pie") {
      return <PieChart items={pieItems} clearItems={() => clearItems("pie")} />;
    }
  };

  return (
    <>
      <Select
        style={{ width: "150px", marginRight: "5px" }}
        value={value}
        options={options}
        onChange={(val) => {
          setItems([]);
          setValue(val);
        }}
      />
      {getChart()}
    </>
  );
};

export default Charts;
