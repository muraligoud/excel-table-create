import React, { useEffect } from "react";
import _ from "lodash";
import c3 from "c3";
import { Result, Tooltip, Button } from "antd";
import { PieChartOutlined, ClearOutlined } from "@ant-design/icons";
import { useValues } from "kea";

import logic from "../../logics/DataLogic";

const PieChart: React.FC<any> = (props) => {
  const { data } = useValues(logic);
  const { items, clearItems } = props;
  const columns: any = _.chain(items)
    .map((item: any) => {
      return [item, ..._.map(data, (val) => val[item])];
    })
    .filter((val) => val !== undefined)
    .value();

  useEffect(() => {
    if (!_.isEmpty(items)) {
      c3.generate({
        bindto: "#chart",
        data: {
          columns,
          type: "pie",
        },
      });
    }
  }, [items, columns, data]);

  return _.isEmpty(items) ? (
    <Result
      icon={<PieChartOutlined />}
      title={`Drop here to create Pie Chart`}
    />
  ) : (
    <>
      <Tooltip title="Clear">
        <Button className={"ant-btn-icon"} shape="circle" onClick={clearItems}>
          <ClearOutlined />
        </Button>
      </Tooltip>
      <div id="chart" />
    </>
  );
};

export default PieChart;
