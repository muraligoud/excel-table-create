import { Switch, Route } from "react-router-dom";

import { Upload, Content } from "../Content";
import { ExcelTable } from "../ExcelTable";

const App = () => {
  return (
    <>
      <Switch>
        <Route exact path="/" component={Upload} />
        <Route exact path="/excel-table" component={ExcelTable} />
        <Route exact path="/create-tables-charts" component={Content} />
      </Switch>
    </>
  );
};

export default App;
