const columns = [
  {
    title: "Sales Order ID",
    dataIndex: "SalesOrderID",
    width: 80,
  },
  {
    title: "Order Date",
    dataIndex: "OrderDate",
    width: 100,
  },
  {
    title: "Ship Date",
    dataIndex: "ShipDate",
    width: 100,
  },
  {
    title: "Account Number",
    dataIndex: "AccountNumber",
    width: 160,
  },
  {
    title: "Product ID",
    dataIndex: "ProductID",
    width: 90,
  },
  {
    title: "Name",
    dataIndex: "Name",
    width: 200,
  },
  {
    title: "Product Sub Category",
    dataIndex: "Product_SUBCategory",
    width: 160,
  },
  {
    title: "List Price",
    dataIndex: "ListPrice",
    width: 90,
  },
  {
    title: "Unit Price",
    dataIndex: "UnitPrice",
    width: 90,
  },
  {
    title: "Order Qty",
    dataIndex: "OrderQty",
    width: 90,
  },
  {
    title: "Standard Cost",
    dataIndex: "StandardCost",
    width: 100,
  },
];

export default columns;
