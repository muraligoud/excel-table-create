import { Table, PageHeader, Space, Typography, Empty } from "antd";
import { ArrowRightOutlined } from "@ant-design/icons";
import { useValues } from "kea";
import { Link, useHistory } from "react-router-dom";
import _ from "lodash";

import logic from "../../logics/DataLogic";

const ExcelTable = () => {
  const { data, title, headerNames: columns } = useValues(logic);
  const history = useHistory();

  return (
    <PageHeader
      title={title ? title : "Table"}
      onBack={() => history.push("/")}
      extra={
        !_.isEmpty(data) && (
          <Space style={{ marginRight: "10px" }}>
            <Typography.Title level={5}>Create Tables/Charts</Typography.Title>
            <Link to="/create-tables-charts">
              <ArrowRightOutlined style={{ fontSize: "20px" }} />
            </Link>
          </Space>
        )
      }
    >
      {_.isEmpty(data) ? (
        <Empty style={{ height: "100vh", color: "black" }} />
      ) : (
        <Table
          dataSource={data}
          columns={columns}
          rowKey={(record) => `${record.SalesOrderID}-${record.ProductID}`}
          bordered
          pagination={{
            position: ["bottomRight"],
            pageSize: 100,
          }}
          scroll={{ y: `calc(100vh - 150px` }}
        />
      )}
    </PageHeader>
  );
};

export default ExcelTable;
