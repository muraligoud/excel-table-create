import React from "react";
import _ from "lodash";
import { useValues, useActions } from "kea";

import logic from "../../logics/DataLogic";

/** DragAndDrop */
const DragAndDrop: React.FC<any> = ({ currentValue, children }) => {
  const { items } = useValues(logic);
  const { setItems } = useActions(logic);

  const handleDragEnter = (e: any) => {
    e.preventDefault();
    if (currentValue) {
      setItems(_.uniq([...items, currentValue]));
    }
  };
  const preventDefault = (e: any) => {
    e.preventDefault();
  };

  return (
    <div
      onDragEnter={preventDefault}
      onDragLeave={preventDefault}
      onDragOver={preventDefault}
      onDrop={handleDragEnter}
    >
      {children}
    </div>
  );
};

export default DragAndDrop;
