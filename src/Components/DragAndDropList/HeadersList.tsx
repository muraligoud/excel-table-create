import { Menu } from "antd";
import _ from "lodash";
import { FolderOpenOutlined } from "@ant-design/icons";

const HeadersList = ({ headerNames, setCurrentValue }: Record<string, any>) => {
  const onDragStart = (evt: any) => {
    setCurrentValue(evt.currentTarget.id);
  };

  return (
    <Menu mode="inline" theme="dark">
      {_.map(headerNames, (header) => (
        <Menu.Item
          icon={<FolderOpenOutlined />}
          draggable={true}
          key={header}
          id={header}
          onDragStart={onDragStart}
          className={"sortable-list"}
        >
          {header}
        </Menu.Item>
      ))}
    </Menu>
  );
};

export default HeadersList;
