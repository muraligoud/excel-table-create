/**
 * Kea redux store
 *
 * @author Karthik <karthik.x@314ecorp.com>
 */

import { resetContext, getContext } from "kea";

resetContext({
  createStore: {},
});

export default getContext().store;
