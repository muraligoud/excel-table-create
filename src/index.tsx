import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import { Provider } from "react-redux";

import App from "./Components/App";
import store from "./Store";
import ErrorBoundary from "./Components/ErrorBoundary";

import "./styles/index";

ReactDOM.render(
  <Router>
    <Provider store={store}>
      <ErrorBoundary>
        <App />
      </ErrorBoundary>
    </Provider>
  </Router>,
  document.getElementById("root")
);
