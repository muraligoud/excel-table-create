import { kea } from "kea";
import _ from "lodash";

const get =
  <T>(key: string, defaults?: T) =>
  (state: T, payload: Record<string, T>): T => {
    return _.get(payload, key, !_.isUndefined(defaults) ? defaults : state);
  };

const logic = kea({
  path: ["views", "data-logic"],
  defaults: {
    data: [],
    title: "",
    items: [],
  },

  actions: {
    setData: (data: any, title: any) => ({ data, title }),
    setItems: (items: string[]) => ({ items }),
    resetStorage: true,
  },

  reducers: {
    data: {
      setData: get("data"),
    },
    title: {
      setData: get("title"),
    },
    items: {
      setItems: get("items"),
      resetStorage: _.stubArray,
    },
  },
  selectors: {
    headerNames: [
      (selectors) => [selectors.data],
      (data: Record<string, any>[]) => {
        return _.chain(data)
          .first()
          .keys()
          .map((col) => ({ title: col, dataIndex: col, width: 120 }))
          .value();
      },
    ],
  },
});

export default logic;
